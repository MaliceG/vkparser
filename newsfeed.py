import requests
import time
from time import sleep
import datetime

class Newsfeed:
    def __init__(self, access_token):
        self._access_token = access_token
        self._verison = '5.69'

    def get_unix_time(self, date):
        return int(time.mktime(datetime.datetime.strptime(date, "%d/%m/%Y").timetuple()))

    def search_news(self, query, date_start = 0): #date_start от текущего времени до date_start будут собираться посты (странно)1513544400
        params = {'q' : query, 'extended' : 1, 'count' : 2} #если параметр не был указан, то берутся данные от текущего момента, до момента, который был сутки назад
        if date_start != 0:
            params['start_time'] = self.get_unix_time(date_start)

        data = []

        while True:
            response_json =  self.send_request('search', params)['response']
            if response_json['count'] == 0:
                break
            try:
                params['start_from'] = response_json['next_from']
            except:
                params.pop('start_from', None)
                params['end_time'] = response_json['items'][-1]['date'] - 1

                #внести фикс, в какой-то момент пропадает поле next_from
                #в таком случае надо изменить end_time на date последнего поста - 1
                #и убрать start_from
                #прекращать работу только тогда, когда последний запрос с атрибутами
                #start_time & end_time вернут 0 записей

            data.extend(response_json['items'])

            print(response_json['total_count'], len(response_json['items']), len(data))
        return data

        # response = self.send_request('seacrh', params)
        pass

    def send_request(self, method, params): # метод для отправки всех запросов, предполагает, что он будет ограничивать количество запросов
        sleep(0.34)
        enter_point = 'https://api.vk.com/method/newsfeed.' + method
        params['access_token'] = self._access_token
        params['v'] = self._verison
        response = requests.get(enter_point, params)
        response_json = response.json()
        if 'error' in response_json.keys():
            print(method, response_json['error']['error_msg'])
            return self.send_request(method, params)
        return response_json