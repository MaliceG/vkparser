# -*-coding: utf-8 -*-

import requests
from time import sleep

class User:
    def __init__(self, access_token):
        self._access_token = access_token
        self._verison = '5.69'

    def get_users_info(self, userIds): #получаю общую информацию по пулу юзеров
        fields = ['bdate', 'career', 'connections', 'counters', 'country', 'domain', 'nickname', 'relation', 'sex', 'interests']
        params = {'user_ids' : ','.join(map(str, userIds))}
        params.update({'fields' : ','.join(map(str, fields))})
        response = self.send_request('users.get', params)
        
        if (response == 'error'):
            return 'error'
        
        return response['response']

    def get_user_info(self, userId): # получаю информацию о юзере + информацию о группах юзера
        fields = ['bdate', 'career', 'connections', 'counters', 'country', 'domain', 'nickname', 'relation', 'sex', 'interests']
        params = {'user_ids' : userId}
        params.update({'fields' : ','.join(map(str, fields))})
        user_info = self.send_request('users.get', params) #получаю список объектов, где всего один объект
        groups_info = self.get_user_groups(userId)
        
        if (user_info == 'error' or groups_info == 'error'): #если один из объектов вернул ошибку, тогда не обрабатываем запрос
            return 'error'
        user_json = user_info['response'][0]
        user_json['groups'] = groups_info
        return user_info

    def get_user_groups(self, userId): # получаю информацию о группах пользователей
        count = 200
        params = {'user_id' : userId, 'extended' : '1', 'count' : count}
        offset = 0
        groups = []
         
        while True:
            params['offset'] = offset
            response = self.send_request('users.getSubscriptions', params)
        
            if (response == 'error'):
                return 'error'

            count_groups = response['response']['count']
            groups.extend(response['response']['items'])
            print('groups', count_groups, len(groups))
        
            if len(groups) >= count_groups or len(response['response']['items']) == 0:
                break

            offset += count
        
        return groups

    def get_25_users(self, users_id):
        fields = "sex, bdate, city, country, online, lists, site, education, status, last_seen, relation, counters, occupation, personal, connections"
        base_string = '\nusers = users + API.users.get({"user_ids" : users_id[users_it], "fields" : "%s"});'
        
        code = 'var users = [];\n'
        code += 'var users_id = [' + ','.join(map(str, users_id)) + '];\n'
        code += 'var users_it = 0;\n'
        code += 'while (users_it < users_id.length) \n{\n'
        code += base_string % (fields)
        code += '\n users_it = users_it + 1; \n };'

        if len(users_id) > 25:
            return;

        code += '\nreturn users;'

        params = {"code" : code}

        return self.send_request("execute", params)['response']

    def get_25_users_groups(self, users_id, count = 50):
        base_string = '\nvar temp = API.groups.get({"user_id" : users_id[users_it], "count" : "%d"});'
        
        code = 'var users_groups = [];\n'
        code += 'var users_id = [' + ','.join(map(str, users_id)) + '];\n'
        code += 'var users_it = 0;\n'
        code += 'while (users_it < users_id.length) \n{\n'
        code += base_string % (count) + '\n';
        code += 'if (temp.length > 0) users_groups = users_groups + temp.items;'
        code += '\n users_it = users_it + 1; \n };\n'

        if len(users_id) > 25:
            return;

        code += '\nreturn users_groups;'

        params = {"code" : code}

        return self.send_request("execute", params)['response']

    def send_request(self, method, params): # метод для отправки всех запросов, предполагает, что он будет ограничивать количество запросов
        sleep(0.34)
        enter_point = 'https://api.vk.com/method/' + method
        params['access_token'] = self._access_token
        params['v'] = self._verison
        response = requests.get(enter_point, params)
        response_json = response.json()
        if 'error' in response_json.keys():
            print(method, response_json['error']['error_msg'])
            return self.send_request(method, params)
        return response_json