# -*-coding: utf-8 -*-

from users import User
from groups import Groups
import datetime
import codecs
import csv
import sys
import os

def write_users_csv(filename, data):
    with open(filename, 'a',newline="\n", encoding='utf8') as f:
        writer = csv.writer(f)

        if os.stat(filename).st_size == 0:
            writer.writerow(('id',
                               'Имя',
                               'Фамилия',
                               'Пол',
                               'Город',
                               'Страна',
                               'Статус',
                               'Онлайн',
                               'Последнее посещение',
                               'Дата рождения',
                               'Кол-во друзей',
                               'Кол-во подписчиков (фолловеры)',
                               'Кол-во музыки',
                               'Кол-во фото',
                               'Кол-во подписок (на людей)',
                               'Кол-во родственников (в вк)',
                               'Отношения',
                               'Интересы',
                               'Род занятия',
                               'Занятие',
                               'Окончание обучения',
                               'skype',
                               'facebook',
                               'facebook_name',
                               'twitter',
                               'instagram',
                               'Источник вдохновения',
                               'Главное в людях',
                               'Главное в жизни',
                               'Отношение к курению',
                               'Отношение к алкоголю',
                               'Политические взгляды',
                               'Языки'))


        writer.writerow( (data['id'],
                          data['first_name'],
                          data['last_name'],
                          data['sex'],
                          data['city'],
                          data['country'],
                          data['status'],
                          data['online'],
                          data['last_seen'],
                          data['bdate'],
                          data['friends_count'],
                          data['followers_count'],
                          data['music_count'],
                          data['photo_count'],
                          data['subscriptions_count'],
                          data['familiars_count'],
                          data['relations'],
                          data['interests'],
                          data['occupation'],
                          data['occupation_name'],
                          data['graduation'],
                          data['skype'],
                          data['facebook'],
                          data['facebook_name'],
                          data['twitter'],
                          data['instagram'],
                          data['inspired_by'],
                          data['people_main'],
                          data['life_main'],
                          data['smoking'],
                          data['alcohol'],
                          data['political'],
                          data['langs']) )


def write_groups_csv(user_id, groups_id):
    with open('csv/users_groups.csv', 'a', newline = "\n", encoding='utf-8') as f:
        writer = csv.writer(f)
        for i in range(len(groups_id)):
            writer.writerow( (user_id,
                              groups_id[i])
            )


def user_to_dict(json_data):

    _map_online = ['Оффлайн', 'В сети']
    _map_relations = ['не указано', 'не женат/не замужем', 'есть друг/есть подруга', 'помолвлен/помолвлена', 'женат/замужем', 'всё сложно', 'в активном поиске', 'влюблён/влюблена', 'в гражданском браке']
    _map_people_main = ['', 'ум и креативность', 'доброта и честность', 'красота и здоровье', 'власть и богатство', 'смелость и упорство', 'юмор и жизнелюбие']
    _map_main_in_life = ['', 'семья и дети', 'карьера и деньги', 'развлечения и отдых', 'наука и исследования', 'совершенствование мира', 'саморазвитие', 'красота и искусство', 'слава и влияние']
    _map_smoking = ['', 'резко негативное', 'негативное', 'компромиссное', 'нейтральное', 'положительное']
    _map_alcohol = ['', 'резко негативное', 'негативное', 'компромиссное', 'нейтральное', 'положительное']
    _map_political = ['', 'коммунистические', 'социалистические', 'умеренные', 'либеральные', 'консервативные', 'монархические', 'ультраконсервативные', 'индифферентные', 'либертарианские']
    _map_occupation = {'work' : 'работа', 'school' : 'среднее образование', 'university' : 'высшее образование'}

    try:
        user_id = json_data['id']
    except:
        user_id = ''

    try:
        first_name = json_data['first_name']
    except:
        first_name = ''

    try:
        last_name = json_data['last_name']
    except:
        last_name = ''

    try:
        sex = 'Ж' if json_data['sex'] == 1 else 'М'
    except:
        sex = ''

    try:
        city = json_data['city']['title']
    except:
        city = ''

    try:
        country = json_data['country']['title']
    except:
        country = ''

    try:
        status = json_data['status']
    except:
        status = ''

    try:
        last_seen = datetime.datetime.fromtimestamp(int(json_data['last_seen']['time'])).strftime('%Y-%m-%d %H:%M:%S')
    except:
        last_seen = ''

    try:
        bdate = json_data['bdate']
    except:
        bdate = ''

    try:
        friends_count = json_data['counters']['friends']
    except:
        friends_count = ''

    try:
        followers_count = json_data['counters']['followers']
    except:
        followers_count = ''

    try:
        music_count = json_data['counters']['audios']
    except:
        music_count = ''

    try:
        photo_count = json_data['counters']['photos']
    except:
        photo_count = ''

    try:
        subscriptions_count = json_data['counters']['subscriptions']
    except:
        subscriptions_count = ''

    try:
        familiars_count = len(json_data['relatives'])
    except:
        familiars_count = ''

    try:
        relations = _map_relations[json_data['relation']]
    except:
        relations = ''

    try: 
        interests =  json_data['interests']
    except:
        interests = ''

    try: 
        online = _map_online[json_data['online']]
    except:
        online = ''

    try: 
        occupation = _map_occupation[json_data['occupation']['type']]
    except:
        occupation = ''

    try: 
        occupation_name = json_data['occupation']['name']
    except:
        occupation_name = ''

    try: 
        graduation = json_data['graduation']
    except:
        graduation = ''

    try: 
        skype = json_data['skype']
    except:
        skype = ''

    try: 
        facebook = json_data['facebook']
    except:
        facebook = ''

    try:
        facebook_name = 'https://www.facebook.com/' + json_data['facebook_name']
    except:
        facebook_name = ''

    try: 
        twitter = 'https://twitter.com/' + json_data['twitter']
    except:
        twitter = ''

    try: 
        instagram = 'instagram.com/' + json_data['instagram']
    except:
        instagram = ''

    try: 
        inspired_by = json_data['personal']['inspired_by']
    except:
        inspired_by = ''

    try: 
        people_main = _map_people_main[json_data['personal']['people_main']]
    except:
        people_main = ''

    try: 
        life_main = _map_main_in_life[json_data['personal']['life_main']]
    except:
        life_main = ''

    try: 
        smoking = _map_smoking[json_data['personal']['smoking']]
    except:
        smoking = ''

    try: 
        alcohol = _map_alcohol[json_data['personal']['alcohol']]
    except:
        alcohol = ''

    try: 
        political = _map_political[json_data['personal']['political']]
    except:
        political = ''

    try: 
        langs = ';'.join(map(str,json_data['personal']['langs']))
    except:
        langs = ''

    user_data = {'id' : user_id,
                'first_name' : first_name,
                'last_name' : last_name,
                'sex' : sex,
                'city' : city,
                'country' : country,
                'status' : status,
                'online' : online,
                'last_seen' : last_seen,
                'bdate' : bdate,
                'friends_count' : friends_count,
                'followers_count' : followers_count,
                'music_count' : music_count,
                'photo_count' : photo_count,
                'subscriptions_count' : subscriptions_count,
                'familiars_count' : familiars_count,
                'relations' : relations,
                'interests' : interests,
                'occupation' : occupation,
                'occupation_name' : occupation_name,
                'graduation' : graduation,
                'skype' : skype,
                'facebook' : facebook,
                'facebook_name' : facebook_name,
                'twitter' : twitter,
                'instagram' : instagram,
                'inspired_by' : inspired_by,
                'people_main' : people_main,
                'life_main' : life_main,
                'smoking' : smoking,
                'alcohol' : alcohol,
                'political' : political,
                'langs' : langs
    }

    return user_data


def get_users_info():
    user = User('')

    members_id = []
    with open('csv/garnier_memb.csv', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            members_id.extend(row)

    # print(members_id[:26])

    users_id = members_id[:25]

    iterator = 0
    while iterator < len(users_id):
        users_json = user.get_25_users(users_id[iterator:iterator+25])
        for user_ in users_json:
            # print(user_)
            user_data = user_to_dict(user_)
            write_users_csv('csv/users.csv', user_data)
            # print(user_data)
        iterator += 25
        print(iterator, ' / ', len(users_id), '------------------------')

def get_users_groups():
    user = User('')

    members_id = []
    with open('csv/garnier_memb.csv', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            members_id.extend(row)

    # print(members_id[:26])

    users_id = members_id
    users_data = []
    iterator = 0
    while iterator < len(users_id):
        users_data += user.get_25_users_groups(users_id[iterator:iterator+25])

        iterator += 25
        print(iterator, ' / ', len(users_id), '------------------------')

    with open('csv/users_groups.csv', 'a',newline="\n", encoding='utf8') as f:
        writer = csv.writer(f)
        writer.writerow( ( users_data ) )


def main():
    if sys.stdout.encoding != 'utf-8':
      sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')

    # get_users_info()
    get_users_groups()


if __name__ == "__main__":
    main()