# -*-coding: utf-8 -*-
import requests
from time import sleep

class Groups:
    """docstring for Groups"""
    def __init__(self, access_token):
        self._access_token = access_token
        self._verison = '5.69'
        
    def get_group_members(self, group_id):
        fields = ['bdate', 'career', 'connections', 'counters', 'country', 'domain', 'nickname', 'relation', 'sex', 'interests']
        members = []
        count = 1000
        params = {'group_id' : group_id, 'count' : count}
        params.update({'fields' : ','.join(map(str, fields))})
        offset = 0

        while True:
            params['offset'] = offset
            response = self.send_request('getMembers', params)
        
            if (response == 'error'):
                return 'error'

            count_posts = response['response']['count']
            members.extend(response['response']['items'])
            print(count_posts, len(members))
        
            if len(members) >= count_posts:
                break

            offset += count

        return members

    def execute_25_groups(self, users_id, count = 200):
        code = 'var groups = [];'
        base_string = '\ngroups = groups + API.users.getSubscriptions({"user_id" : %d, "count" : %d, "extended" : 1}).items;'

        if len(users_id) > 25:
            users_count = 25
        else:
            users_count = len(users_id)

        for i in range(users_count):
            code += base_string % (users_id[i], count)

        code += '\nreturn groups;'

        print(code)

    def send_request(self, method, params): # метод для отправки всех запросов, предполагает, что он будет ограничивать количество запросов
            sleep(0.34)
            enter_point = 'https://api.vk.com/method/groups.' + method
            params['access_token'] = self._access_token
            params['v'] = self._verison
            response = requests.get(enter_point, params)
            response_json = response.json()
            if 'error' in response_json.keys():
                print(method, response_json['error']['error_msg'])
                return self.send_request(method, params)
            return response_json